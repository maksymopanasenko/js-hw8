// #1

const paragrafs = document.querySelectorAll('p');

paragrafs.forEach(paragraf => paragraf.style.background = '#ff0000');

// #2

const optionsList = document.getElementById('optionsList');

console.log(optionsList);

const parentOptinsList = optionsList.parentElement;

console.log(parentOptinsList);

const childOptionsList = optionsList.childNodes;

if (childOptionsList) {
    childOptionsList.forEach(node => {
        console.log(`Name of node: ${node.nodeName}, type of node: ${node.nodeType}`);
    });   
}

// #3

const testParagraphElement = document.querySelector('#testParagraph');

testParagraphElement.textContent = 'This is a paragraph';

// #4-5

const mainHeaderElements = document.querySelector('.main-header').children;

for (let key of mainHeaderElements) {
    console.log(key);
    key.classList.add('nav-item');
}

// #6

const sectionTitleElements = document.querySelectorAll('.section-title');

sectionTitleElements.forEach(elem => elem.classList.remove('section-title'));